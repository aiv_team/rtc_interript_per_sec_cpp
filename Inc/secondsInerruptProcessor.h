#pragma once
#include "main.h"

namespace siProcessor
{
    class secondsInterruptProcessor
    {
    private:
        GPIO_TypeDef *port;
        uint16_t pin;
        uint8_t delay;
        uint8_t counter;

    public:
        void init(GPIO_TypeDef *, uint16_t);
        void setDelay(uint8_t);
        void process();
    };
}
#include "secondsInerruptProcessor.h"

using namespace siProcessor;

void secondsInterruptProcessor::init(GPIO_TypeDef *port, uint16_t pin)
{
    secondsInterruptProcessor::port = port;
    secondsInterruptProcessor::pin = pin;
    secondsInterruptProcessor::delay = 1;
    secondsInterruptProcessor::counter = 1;
}

void secondsInterruptProcessor::setDelay(uint8_t delay)
{
    secondsInterruptProcessor::delay = delay;
    secondsInterruptProcessor::counter = delay;
}

void secondsInterruptProcessor::process()
{
    if (!(--secondsInterruptProcessor::counter)) {
        HAL_GPIO_TogglePin(secondsInterruptProcessor::port, secondsInterruptProcessor::pin);
        secondsInterruptProcessor::counter = secondsInterruptProcessor::delay;
    }
}
